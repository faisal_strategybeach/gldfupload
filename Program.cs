﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Net;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;

namespace glDFUpload_V2
{
    public class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            SqlConnection con;
            SqlDataReader reader;
            try
            {
                WriteToFile(" ");
                WriteToFile("----------- Started on " + DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt") + " ------------");

                con = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnString"]));
                con.Open();

                try
                {
                    SqlCommand cmd = new SqlCommand(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["procName"]), con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteReader();
                    con.Close();
                }
                catch (Exception ex)
                {
                    WriteToFile(ex.Message);
                }

                con.Open();
                string strQuery = "select ISNULL(SMVAUserId, '') SMVAUserId, ISNULL(SMVAPassword, '') SMVAPassword, CAST(ISNULL(SMVAExpireDate,'') AS VARCHAR) SMVAExpireDate, ISNULL(SMVAAcctType, '') SMVAAcctType, ISNULL(SMVAFN, '') SMVAFN, ISNULL(SMVALN, '') SMVALN, ISNULL(SMVAAdd1, '') SMVAAdd1, ISNULL(SMVAAdd2, '') SMVAAdd2, ISNULL(SMVACity, '') SMVACity, ISNULL(SMVAState, '') SMVAState, ";
                strQuery += "ISNULL(SMVAZip, '') SMVAZip, ISNULL(SMVAFC, '') SMVAFC, ISNULL(SMVASubAcctCode, '') SMVASubAcctCode, ISNULL(SMVAEmailAddr, '') SMVAEmailAddr, ISNULL(SMVBAdd1, '') SMVBAdd1, ISNULL(SMVBAdd2, '') SMVBAdd2, ISNULL(SMVBCity, '') SMVBCity, ISNULL(SMVBState, '') SMVBState, ISNULL(SMVBZip, '') SMVBZip, ISNULL(SMVBFC, '') SMVBFC, ISNULL(SMVAAcctNo, '') SMVAAcctNo from "
                    + Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["tableName"]) + " WHERE 1=1 ";

                strQuery += Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["queryFilter"]);
                reader = new SqlCommand(strQuery, con).ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        try
                        {
                            string strLog = "SMVAUserId: " + reader.GetString(0);
                            strLog += ", SMVAPassword: " + reader.GetString(1);
                            strLog += ", SMVAExpireDate: " + reader.GetString(2);
                            strLog += ", SMVAAcctType: " + reader.GetString(3);
                            strLog += ", SMVAFN: " + reader.GetString(4);
                            strLog += ", SMVALN: " + reader.GetString(5);
                            strLog += ", SMVAAdd1: " + reader.GetString(6);
                            strLog += ", SMVAAdd2: " + reader.GetString(7);
                            strLog += ", SMVACity: " + reader.GetString(8);
                            strLog += ", SMVAState: " + reader.GetString(9);
                            strLog += ", SMVAZip: " + reader.GetString(10);
                            strLog += ", SMVAFC: " + reader.GetString(11);
                            strLog += ", SMVASubAcctCode: " + reader.GetString(12);
                            strLog += ", SMVAEmailAddr: " + reader.GetString(13);
                            strLog += ", SMVBAdd1: " + reader.GetString(14);
                            strLog += ", SMVBAdd2: " + reader.GetString(15);
                            strLog += ", SMVBCity: " + reader.GetString(16);
                            strLog += ", SMVBState: " + reader.GetString(17);
                            strLog += ", SMVBZip: " + reader.GetString(18);
                            strLog += ", SMVBFC: " + reader.GetString(19);

                            //Added On 13 Dec, 2017
                            strLog += ", SMVAAcctNo: " + reader.GetString(20);

                            WriteToFile(strLog);

                            using (var client = new WebClient())
                            {
                                var data = new System.Collections.Specialized.NameValueCollection();

                                //Changed On 13 Dec, 2017       //if userId is not available then use account no.
                                if (reader.GetString(0).Trim() == string.Empty)
                                    data.Add("SMVAUserId", reader.GetString(20));
                                else
                                    data.Add("SMVAUserId", reader.GetString(0));

                                if (reader.GetString(1).Trim() == string.Empty)
                                    data.Add("SMVAPassword", reader.GetString(20));
                                else
                                    data.Add("SMVAPassword", reader.GetString(1));

                                data.Add("SMVAExpireDate", reader.GetString(2));

                                data.Add("SMVAAcctType", reader.GetString(3));
                                data.Add("SMVAFN", reader.GetString(4));
                                data.Add("SMVALN", reader.GetString(5));

                                data.Add("SMVAAdd1", reader.GetString(6));
                                data.Add("SMVAAdd2", reader.GetString(7));
                                data.Add("SMVACity", reader.GetString(8));

                                data.Add("SMVAState", reader.GetString(9));
                                data.Add("SMVAZip", reader.GetString(10));
                                data.Add("SMVAFC", reader.GetString(11));

                                //Added On 08 Jun, 2016
                                data.Add("SMVASubAcctCode", reader.GetString(12));
                                data.Add("SMVAEmailAddr", reader.GetString(13));
                                data.Add("SMVBAdd1", reader.GetString(14));
                                data.Add("SMVBAdd2", reader.GetString(15));
                                data.Add("SMVBCity", reader.GetString(16));
                                data.Add("SMVBState", reader.GetString(17));
                                data.Add("SMVBZip", reader.GetString(18));
                                data.Add("SMVBFC", reader.GetString(19));

                                //Added On 13 Dec, 2017
                                data.Add("SMVAAcctNo", reader.GetString(20));

                                var result = client.UploadValues(System.Configuration.ConfigurationManager.AppSettings["URLAddress"], data);

                                string str = System.Text.Encoding.UTF8.GetString(result);
                                WriteToFile("Response: " + str.Trim());

                                //Added On 13 Dec, 2017
                                string notes = "";
                                if (reader.GetString(0).Trim() == string.Empty)
                                    notes = "UserId, ";
                                if (reader.GetString(1).Trim() == string.Empty)
                                    notes += "Password";

                                if (notes != string.Empty)
                                    WriteToFile("Note: " + notes.Trim(' ', ',') + " not available");

                                WriteToFile("");
                            }
                        }
                        catch (WebException ex)
                        {
                            WriteToFile(ex.Message);
                            WriteToFile("");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                WriteToFile(ex.Message);
                WriteToFile("");
            }

            WriteToFile("----------- Finished on " + DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt") + " ------------");
        }

        public static void WriteToFile(string message)
        {
            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            try
            {
                if (File.Exists(Convert.ToString(ConfigurationManager.AppSettings["LogFilePath"]) + "glDFUpload_" + DateTime.Now.ToString("MMddyy") + ".txt"))
                    ostrm = new FileStream(Convert.ToString(ConfigurationManager.AppSettings["LogFilePath"]) + "glDFUpload_" + DateTime.Now.ToString("MMddyy") + ".txt", FileMode.Append, FileAccess.Write);
                else
                    ostrm = new FileStream(Convert.ToString(ConfigurationManager.AppSettings["LogFilePath"]) + "glDFUpload_" + DateTime.Now.ToString("MMddyy") + ".txt", FileMode.Create, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open log file for writing");
                Console.WriteLine(e.Message);
                return;
            }

            Console.SetOut(writer);
            Console.WriteLine(message);
            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
        }
    }
}
